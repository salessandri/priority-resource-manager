/*
 * Copyright (c) 2013, Santiago Alessandri
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 */
 
#ifndef __RESOURCE_MANAGER_H__
#define __RESOURCE_MANAGER_H__

#include <stdint.h>
#include <pthread.h>

/**
 * \brief Structure to handle the threads waiting for the resource.
 * 
 * There will exist one of this structures for each of the priorities
 * of the threads that are waiting for the resource.
 * When there are no threads of a given priority the structure is
 * eliminated.
 * 
 */
typedef struct resource_waiting_threads {
    /**
     * \brief The priority being handled by the structure.
     * 
     */
    int priority;
    
    /**
     * \brief pthread_cond_t where the threads of the priority of the structure will wait on.
     * 
     */
    pthread_cond_t condition_variable;
    
    /**
     * \brief Number of threads waiting on the condition variable.
     * 
     */
    uint32_t count;
    
    /**
     * \brief Pointer used to make this structure usable by utlist.
     */
    struct resource_waiting_threads *next;
} resource_waiting_threads;

/**
 * \brief Structure used to handle the resource.
 * 
 * It is the only exported structure of the library. 
 * Each resource is represented and handled by using one of this structures.
 * 
 * In escence it is a monitor. All the methods that are called to make use
 * of the resource must acquire the resource's mutex.
 * 
 */
typedef struct resource_struct {
    /**
     * \brief Resource's mutex. It is used to guarantee single thread access to the structure.
     * 
     */
    pthread_mutex_t resource_mutex;
    
    /**
     * \brief Pointer to the original resource. The one given to the resource_init function.
     * 
     */
    void *original_resource;
    
    /**
     * \brief Pointer to the current valid resource.
     * 
     */
    void *current_resource;
    
    /**
     * \brief Pointer to the function used to generate the copy of the resource.
     * 
     * The signature of the function must be (void*)(void*).
     * The function is responsible for allocating memory for the copy and initialize it.
     * The returned pointer must point to the initialized copy.
     * 
     */
    void* (*copy_resource)(void*);
    
    /**
     * \brief Priority ordered list of the waiting thread queues.
     * 
     */
    resource_waiting_threads *threads_waiting_list;
    
    /**
     * \brief Flag indicating whether the resource is owned by a thread or not.
     * 
     */
    int is_owned;
    
    /**
     * \brief pthread_t of the current owner of the resource.
     * 
     */
    pthread_t owner;
    
} Resource;

/**
 * \brief Initialize the corresponding Resource.
 * 
 * It sets up the correct value to the different fields.
 * 
 * \param resource Pointer to the Resource being initialized.
 * \param original Pointer to the content to be managed.
 * \param copy_function Pointer to the copy function to be used.
 */
void resource_init(Resource *resource, void* original, void* (*copy_function)(void*));

/**
 * \brief Lock the resource and return a pointer to the content to use.
 * 
 * A copy of the resource is generated and given to the thread to use.
 * In case the Resource is owned by a thread with the same or higher priority the
 * thread is delayed.
 * 
 * \param resource Pointer to the Resource wanted to be locked.
 * \return A pointer to the content of the resource to use.
 */
void* resource_lock(Resource* resource);

/**
 * \brief Unlock the resource and return whether the resource has been correctly updated.
 * 
 * This method is called when the resource has been used and it is to be released.
 * If another thread took control of the resource while the calling thread was using it
 * the update will fail and the resource_content will be freed.
 * 
 * \param resource Pointer to the Resource that is being released.
 * \param resource_content Pointer to the resource that was given to the thread to work with.
 * \return 0 in case the releasing was successful or 1 in case another thread preempted the resource.
 */
int resource_unlock(Resource* resource, void* resource_content);

/**
 * \brief Tear down the Resource handling structure and release the resource from the manager's control.
 * 
 * This method releases the real resource and destroy the management control structure.
 * It returns the final real resource, which can differ from the original resource given.
 * 
 * \param resource Pointer to the Resource being destroyed.
 * \return A pointer to the current real resource.
 */
void* resource_destroy(Resource* resource);

#endif