/*
 * Copyright (c) 2013, Santiago Alessandri
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 */

#ifndef __RESOURCE_MANAGER_HELPER_H__
#define __RESOURCE_MANAGER_HELPER_H__

#include <stdlib.h>
#include <string.h>

#define CREATE_BUFFER_COPY_FUNCTION(func_name, buffer_len) \
    void* func_name(void* arg1) { \
        void* new_buffer = malloc(buffer_len); \
        memcpy(new_buffer, arg1, buffer_len); \
        return new_buffer; \
    }

#define CREATE_STRUCT_COPY_FUNCTION(func_name, structure) \
    void* func_name(void* arg1) { \
        structure* new_buffer = (structure*)malloc(sizeof(structure)); \
        *new_buffer = *(structure*)arg1; \
        return new_buffer; \
    }

void* resource_helper_copy_int(void* arg1);

void* resource_helper_copy_long(void* arg1);

void* resource_helper_copy_float(void* arg1);

void* resource_helper_copy_double(void* arg1);

void* resource_helper_copy_str(void* arg1);

#endif