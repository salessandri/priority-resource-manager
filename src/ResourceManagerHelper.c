/*
 * Copyright (c) 2013, Santiago Alessandri
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 */

#include "ResourceManagerHelper.h"

void* resource_helper_copy_int(void* arg1)
{
    int* new_int = (int*)malloc(sizeof(int));
    *new_int = *(int*)arg1;
    return new_int;
}

void* resource_helper_copy_long(void* arg1)
{
    long* new_long = (long*)malloc(sizeof(long));
    *new_long = *(long*)arg1;
    return new_long;
}

void* resource_helper_copy_float(void* arg1)
{
    float* new_float = (float*)malloc(sizeof(float));
    *new_float = *(float*)arg1;
    return new_float;
}

void* resource_helper_copy_double(void* arg1)
{
    double* new_double = (double*)malloc(sizeof(double));
    *new_double = *(double*)arg1;
    return new_double;
}

void* resource_helper_copy_str(void* arg1)
{
    char* new_str = (char*)malloc((strlen((char*)arg1) + 1) * sizeof(char));
    strcpy(new_str, (char*)arg1);
    return new_str;
}