/*
 * Copyright (c) 2013, Santiago Alessandri
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 */
 
#include <stdio.h>
#include <stdlib.h>

#include <assert.h>
#include <sched.h>
#include <pthread.h>

#include "utlist.h"

#include "ResourceManager.h"

void resource_init(Resource *resource, void* original, void* (*copy_function)(void*))
{
    pthread_mutexattr_t mutex_attr;
    pthread_mutexattr_init(&mutex_attr);
    pthread_mutexattr_setprotocol(&mutex_attr, PTHREAD_PRIO_INHERIT);
    pthread_mutex_init(&resource->resource_mutex, &mutex_attr);
    pthread_mutexattr_destroy(&mutex_attr);
    
    resource->original_resource = original;
    resource->current_resource = original;
    resource->copy_resource = copy_function;
    resource->threads_waiting_list = NULL;
    resource->is_owned = 0;
}

int resource_waiting_threads_cmp(resource_waiting_threads* a, resource_waiting_threads* b)
{
    return b->priority - a->priority;
}

void resource_wait_thread(Resource *resource, int thread_prio)
{
    resource_waiting_threads *prio_waiting_threads = NULL;
    LL_SEARCH_SCALAR(resource->threads_waiting_list, prio_waiting_threads, priority, thread_prio);
    // If there are no threads of the current thread priority waiting, create a new waiting queue
    // for the priority.
    if (prio_waiting_threads == NULL) {
        prio_waiting_threads = (resource_waiting_threads*)malloc(sizeof(resource_waiting_threads));
        prio_waiting_threads->priority = thread_prio;
        prio_waiting_threads->count = 0;
        pthread_cond_init(&prio_waiting_threads->condition_variable, NULL);
        LL_PREPEND(resource->threads_waiting_list, prio_waiting_threads);
        LL_SORT(resource->threads_waiting_list, resource_waiting_threads_cmp);
    }
    
    // Wait on the condition variable, updating the count of threads waiting.
    prio_waiting_threads->count++;
    pthread_cond_wait(&prio_waiting_threads->condition_variable, &resource->resource_mutex);
    prio_waiting_threads->count--;
    
    // If there are no threads left of the current priority waiting, delete the waiting queue
    if (prio_waiting_threads->count == 0) {
        pthread_cond_destroy(&prio_waiting_threads->condition_variable);
        LL_DELETE(resource->threads_waiting_list, prio_waiting_threads);
        free(prio_waiting_threads);
    }
}

void resource_resolv_conflict(Resource* resource, pthread_t current_thread, struct sched_param *current_thread_sched_params)
{
    struct sched_param owner_sched_params;
    int owner_thread_policy;
    if (pthread_getschedparam(resource->owner, 
                              &owner_thread_policy, 
                              &owner_sched_params) != 0) {
        perror("[-] Could not obtain owner thread sched parameters\n");
        exit(-1);
    }
    
    
    // Owner's prio >= Requester's prio -> Requester must wait
    if (owner_sched_params.sched_priority >= current_thread_sched_params->sched_priority) {
        resource_wait_thread(resource, current_thread_sched_params->sched_priority);
    }
    // Owner's prio < Requester's prio -> Requester is given the resource, Owner is kicked.
    else {
        resource->is_owned = 0;
    }
}

void* resource_lock(Resource* resource)
{
    pthread_t current_thread = pthread_self();
    
    struct sched_param current_thread_sched_params;
    int current_thread_policy;
    if (pthread_getschedparam(current_thread, 
                              &current_thread_policy, 
                              &current_thread_sched_params) != 0) {
        perror("[-] Could not obtain current thread sched parameters\n");
        pthread_mutex_unlock(&resource->resource_mutex);
        exit(-1);
    }
    
    pthread_mutex_lock(&resource->resource_mutex);
    // Wait for the resource to be free.
    while (resource->is_owned) {
        resource_resolv_conflict(resource, current_thread, &current_thread_sched_params);
    }
    
    // Make a copy of the resource
    void *return_value = resource->copy_resource(resource->current_resource);
    
    // Update the Resource status and return the pointer to the content.
    resource->is_owned = 1;
    resource->owner = current_thread;
    
    pthread_mutex_unlock(&resource->resource_mutex);
    return return_value;
}

void wakeup_next_thread(Resource* resource)
{
    // Wake up one thread from the first waiting queue (highest prio)
    if (resource->threads_waiting_list != NULL) {
        pthread_cond_signal(&resource->threads_waiting_list->condition_variable);
    }
}

int resource_unlock(Resource* resource, void* resource_content)
{
    pthread_t current_thread = pthread_self();
    int return_value;
    pthread_mutex_lock(&resource->resource_mutex);
    
    // If the thread is the current owner, noone preemted him
    if (current_thread == resource->owner) {
        // Free the current reason except it is the original
        if (resource->current_resource != resource->original_resource)
            free(resource->current_resource);
        // Update the current version of the resource, set the resource as free and wake up the next thread.
        resource->current_resource = resource_content;
        resource->is_owned = 0;
        return_value = 0;
        wakeup_next_thread(resource);
    }
    // If the thread was preempted, free the copy and return that it was preempted
    else {
        free(resource_content);
        return_value = 1;
    }
    pthread_mutex_unlock(&resource->resource_mutex);
    return return_value;
}

void* resource_destroy(Resource* resource)
{
    pthread_mutex_destroy(&resource->resource_mutex);
    return resource->current_resource;
}